FUNCTION null_point, br, binxy_null_point=binxy_null_point, binxy_mpil=binxy_mpil, extend=extend, mpil_info=mpil_info, mpil_xy=mpil_xy

;+
; Name: null_point.pro
;
; Purpose: finds magnetic null points from 3D potential magnetic field data.
;
; Calling Sequence:
;   output_null_point=null_point(br[,binxy_null_point=binxy_null_point][,binxy_mpil=binxy_mpil][,extend=extend])
;
; Input parameters:
;   br - hmi_sharp_cea_720s Br image
;
; Optional Keyword Parameters:
;   binxy_null_point - number for xy spatial binning of the sharp magnetic field input data [in pixels] ; default=2
;   binxy_mpil - number for xy spatial binning of sharp magnetic field input data related to MPIL detection [pixels] ; default=2
;   extend - length of a square box surrounding each of MPIL pixels [Mm]. The box region will be considered for calculating properties of null points therein; default=5
;   
; Optional Output Keyword Parameters:
;   mpil_info - information on types of MPILs
;   mpil_xy - x and y coordinates of MPILs [pixels]
;   
; Output Parameters:
;   output_null_point - a structure having 3 tags related to the properties of null points
;                       n_np: total number of null points in the height range of 2 to 100 Mm
;                       n_np_2_10: total number of null points in the height range of 2 to 10 Mm     
;                       n_np_10_100: total number of null points in the height range of 10 to 100 Mm
;                    
; Modificatioin History:
;           09-Jun-2017 - Sung-Hong Park - written
;-

IF NOT ISVALID(binxy_null_point) THEN binxy_null_point=2
IF NOT ISVALID(binxy_mpil) THEN binxy_mpil=1
IF NOT ISVALID(extend) THEN extend=5.

extendpix=FIX(extend/0.36442476) ; pixels
bz_height=ROUND(100./(0.36442476*binxy_null_point)) ; Mm

np_heigh_level0=ROUND((2.-0.36442476*binxy_null_point/2.)/(0.36442476*binxy_null_point))
np_heigh_level1=ROUND((10.-0.36442476*binxy_null_point/2.)/(0.36442476*binxy_null_point))

output_np=FLTARR(4)

s0=SIZE(br)
br=CONGRID(br, s0[1]/binxy_null_point*binxy_null_point, s0[2]/binxy_null_point*binxy_null_point)
br=REBIN(br, s0[1]/binxy_null_point, s0[2]/binxy_null_point)

B_LFF, br, FINDGEN(bz_height), bx, by, bz, /seehafer

s1=SIZE(bx)
nx=s1[1]
ny=s1[2]
nz=s1[3]

np_pos=FIND_AR_NULL_POINT(bx, by, bz, binxy=binxy_null_point)

ss=WHERE(np_pos[*,*,np_heigh_level0:*] EQ 1, n_np)

ss_2_10=WHERE(np_pos[*,*,np_heigh_level0:np_heigh_level1] EQ 1, n_np_2_10)
ss_10_100=WHERE(np_pos[*,*,np_heigh_level1+1:*] EQ 1, n_np_10_100)

ss_2_10_mpil=-1
ss_10_100_mpil=-1

IF (N_ELEMENTS(br[*,0])/binxy_mpil GT 8) THEN BEGIN
  IF NOT ISVALID(mpil_info) OR NOT ISVALID(mpil_xy) THEN GET_MPIL_REGION, br, mpil_info, mpil_xy, binxy=binxy_mpil

  IF FINITE(mpil_xy[0]) THEN BEGIN

    tmp = WHERE(FINITE(mpil_info.n) AND (mpil_info.typ LE 1), n_mpil)

    IF n_mpil GE 1 THEN BEGIN
    
      FOR l=0, n_mpil-1 DO BEGIN

        mpil_l_x = REFORM(mpil_xy[0, mpil_info[tmp[l]].offset : mpil_info[tmp[l]].offset + mpil_info[tmp[l]].n - 1])
        mpil_l_y = REFORM(mpil_xy[1, mpil_info[tmp[l]].offset : mpil_info[tmp[l]].offset + mpil_info[tmp[l]].n - 1])

        n_mpil_l=N_ELEMENTS(mpil_l_x)

        FOR ll=0, n_mpil_l-1 DO BEGIN

          x0=FIX(ROUND(1.*mpil_l_x[ll]/binxy_null_point*binxy_mpil-extendpix))>0<(nx-1)
          x1=FIX(ROUND(1.*mpil_l_x[ll]/binxy_null_point*binxy_mpil+extendpix))>0<(nx-1)
          y0=FIX(ROUND(1.*mpil_l_y[ll]/binxy_null_point*binxy_mpil-extendpix))>0<(ny-1)
          y1=FIX(ROUND(1.*mpil_l_y[ll]/binxy_null_point*binxy_mpil+extendpix))>0<(ny-1)
       
          ss_2_10_prev=WHERE(np_pos[x0:x1,y0:y1,np_heigh_level0:np_heigh_level1] EQ 1, n_np_2_10_prev)
          np_pos[x0:x1,y0:y1,np_heigh_level0:np_heigh_level1]=-1.
          ss_10_100_prev=WHERE(np_pos[x0:x1,y0:y1,np_heigh_level1+1:*] EQ 1, n_np_10_100_prev)
          np_pos[x0:x1,y0:y1,np_heigh_level1+1:*]=-1.
          IF (n_np_2_10_prev+n_np_10_100_prev) GE 1 THEN BEGIN
            ss_2_10_mpil=[ss_2_10_mpil,ss_2_10_prev]
            ss_10_100_mpil=[ss_10_100_mpil,ss_10_100_prev]
          ENDIF
        
        ENDFOR   
        
      ENDFOR  

      ss_2_10_mpil=ss_2_10_mpil[UNIQ(ss_2_10_mpil, SORT(ss_2_10_mpil))]
      ss_10_100_mpil=ss_10_100_mpil[UNIQ(ss_10_100_mpil, SORT(ss_10_100_mpil))]

      tmp2=WHERE(ss_2_10_mpil ge 0, n_np_mpil_2_10)
      tmp3=WHERE(ss_10_100_mpil ge 0, n_np_mpil_10_100)
      n_np_mpil=n_np_mpil_2_10+n_np_mpil_10_100
      
      output_null_point={n_np:n_np, n_np_2_10:n_np_2_10, n_np_10_100:n_np_10_100, n_np_mpil:n_np_mpil, n_np_mpil_2_10:n_np_mpil_2_10, n_np_mpil_10_100:n_np_mpil_10_100}

    ENDIF ELSE BEGIN

      output_null_point={n_np:n_np, n_np_2_10:n_np_2_10, n_np_10_100:n_np_10_100, n_np_mpil:0., n_np_mpil_2_10:0., n_np_mpil_10_100:0.}

    ENDELSE

  ENDIF ELSE BEGIN
    
    output_null_point={n_np:n_np, n_np_2_10:n_np_2_10, n_np_10_100:n_np_10_100, n_np_mpil:0., n_np_mpil_2_10:0., n_np_mpil_10_100:0.}

  ENDELSE
   
ENDIF ELSE BEGIN
  
  output_null_point={n_np:n_np, n_np_2_10:n_np_2_10, n_np_10_100:n_np_10_100, n_np_mpil:0., n_np_mpil_2_10:0., n_np_mpil_10_100:0.}
  
ENDELSE

RETURN, output_null_point
 
END